package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {
	private LoginValidator(){}

	public static boolean isValidLoginName( String loginName ) {
		Pattern pattern = Pattern.compile("^[a-z][a-z|0-9]{5}.*", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(loginName);

		return matcher.find();
	}
}
