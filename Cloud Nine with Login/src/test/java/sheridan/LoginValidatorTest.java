package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	@Test
	public void testIsValid_length_BoundryIn() {
	    assertTrue("Invalid login", LoginValidator.isValidLoginName("Benjam"));
	}
	@Test
	public void testIsValid_length_BoundryOut() {
		assertFalse("Valid login", LoginValidator.isValidLoginName("Benja"));
	}
	@Test
	public void testIsValid_length_Exception() {
	    assertFalse("Valid login", LoginValidator.isValidLoginName("Ben"));
	}
	@Test
	public void testIsValid_length_Regular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("Benjamin"));
	}

	@Test
	public void testIsValid_chars_BoundryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("3enjamin"));
	}
	@Test
	public void testIsValid_chars_BoundryIn() {
	    assertTrue("Invalid login", LoginValidator.isValidLoginName("B3njamin"));
	}
	@Test
	public void testIsValid_chars_Regular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("Benjam1n"));
	}
	@Test
	public void testIsValid_chars_Exception() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("Benj@min"));
	}

}
